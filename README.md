# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions
   Run the command ```mvn clean package``` to build a jar in target folder, 
   followed by ```java -jar -D"spring-boot.run.profiles"=dev <fileName>.jar```

   For local, application run refernce, ```mvn spring-boot:run -D"spring-boot.run.profiles"=dev``` 
   For running in docker, ```docker run -e "SPRING_PROFILES_ACTIVE=dev" -p 80:8080 -t red_enrollment:v0.0.1```
