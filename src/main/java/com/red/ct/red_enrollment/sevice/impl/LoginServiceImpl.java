package com.red.ct.red_enrollment.sevice.impl;

import com.red.ct.data.dao.ICustomerDetailDAO;
import com.red.ct.data.dao.IDeliveryPersonDetailDAO;
import com.red.ct.data.dao.IRestaurantDetailDAO;
import com.red.ct.data.model.Base;
import com.red.ct.data.model.CustomerDetail;
import com.red.ct.data.model.DeliveryPersonDetail;
import com.red.ct.data.model.RestaurantDetail;
import com.red.ct.domain.inbound.request.LoginCheckRequest;
import com.red.ct.domain.inbound.request.LoginRequest;
import com.red.ct.domain.inbound.response.ApiResponse;
import com.red.ct.domain.inbound.response.LoginCheckResponse;
import com.red.ct.domain.inbound.response.LoginResponse;
import com.red.ct.domain.outbound.request.D7SendSmsRequest;
import com.red.ct.domain.outbound.response.D7SendSmsResponse;
import com.red.ct.exception.InvalidPayloadException;
import com.red.ct.exception.InvalidXChannelException;
import com.red.ct.red_enrollment.helper.OtpUtil;
import com.red.ct.red_enrollment.service.ILoginService;
import com.red.ct.utility.HeaderValidation;
import com.red.ct.utility.JwtOperation;
import com.red.ct.utility.RestExchangeUtil;
import com.red.ct.utility.constants.*;
import com.red.ct.utility.validator.ContactNumberValidator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.text.MessageFormat;
import java.time.Instant;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class LoginServiceImpl implements ILoginService {

    @Autowired
    private Validator validator;

    @Autowired
    private ICustomerDetailDAO customerDetailDAO;

    @Autowired
    private IRestaurantDetailDAO restaurantDetailDAO;

    @Autowired
    private IDeliveryPersonDetailDAO deliveryPersonDetailDAO;

    @Autowired
    private JwtOperation jwtOperation;

    @Autowired
    private ContactNumberValidator contactNumberValidator;

    @Autowired
    private HeaderValidation headerValidation;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private RestExchangeUtil restExchangeUtil;

    @Autowired
    private OtpUtil otpUtil;

    /**
     * Indicates OTP timeout value post which, value will be removed from the cache
     */
    @Value("${otp.expire : 2}")
    private int otpExpireInMin;

    /**
     * Indicates application Name which will be present in message
     */
    @Value("${otp.appName}")
    private String otpAppName;

    /**
     * Indicates sms triggering api
     */
    @Value("${otp.api}")
    private String otpApi;

    /**
     * Indicates sms triggering api authentication header key
     */
    @Value("${otp.header.key}")
    private String otpApiKeyName;

    /**
     * Indicates sms triggering api authentication header value
     */
    @Value("${otp.header.value}")
    private String otpApiKeyValue;

    @Override
    public ResponseEntity<ApiResponse> processLogin (LoginRequest request, HttpHeaders headers) throws Exception {
        log.debug("Entering LoginServiceImpl.processLogin method");
        ResponseEntity<ApiResponse> response = null;
        headerValidation.validateRequestHeader(headers, RequestType.LOGIN);
        validateCreateConsumerRequest(request);
        String cipherForOTP = (String) redisTemplate.opsForValue().get(request.getRefId());
        redisTemplate.delete(request.getRefId());
        if(StringUtils.isEmpty(cipherForOTP)){
            throw new InvalidPayloadException(ApplicationMessage.INVALID_REF_ID_AND_OTP_COMBINATION);
        }
        if(StringUtils.equalsAnyIgnoreCase(request.getPhoneNo(),
                otpUtil.decryptOTPCipher(cipherForOTP, request.getOTP()))
                && (StringUtils.trimToEmpty(request.getOTP()).length() == Constant.OTP_LENGTH)) {
            response = getTokenResponse(request, headers.getFirst(Header.X_CHANNEL.getValue()));
        } else {
            throw new InvalidPayloadException(ApplicationMessage.INVALID_REF_ID_AND_OTP_COMBINATION);
        }
        log.debug("Exiting LoginServiceImpl.processLogin method");
    	return response;
    }

    @Override
    public ResponseEntity<ApiResponse> processLoginCheck (LoginCheckRequest request, HttpHeaders headers) throws Exception {
        log.debug("Entering LoginServiceImpl.processLoginCheck method");
        // Validating request header & content
        headerValidation.validateRequestHeader(headers, RequestType.LOGIN_CHECK);
        validateLoginCheckRequest(request);
        // Fetch user details, based on xChannel
        Base detailFromDB = getUserDetail(request.getPhoneNo(), headers.getFirst(Header.X_CHANNEL.getValue()));
        if(null == detailFromDB || !detailFromDB.isActive()){
            throw new InvalidPayloadException(ApplicationMessage.USER_NOT_FOUND_OR_INACTIVE);
        }
        log.debug("Payload received for LoginCheckRequest : {}", request);

        // Generating otp & pushing to redis
        String otp = String.format("%04d", new Random().nextInt(10000));
        String cipherText = otpUtil.cipherForOTPWithPhoneNo(request.getPhoneNo(), otp);
        String uuid = UUID.randomUUID().toString();
        redisTemplate.opsForValue().set(uuid, cipherText, otpExpireInMin, TimeUnit.MINUTES);
        log.debug("OTP generated for the request id - {}", uuid);

        // Executing webClient for otp trigger
        String otpMsg = MessageFormat.format(Constant.OTP_MSG, otp, otpAppName, String.valueOf(otpExpireInMin));
        D7SendSmsRequest smsRequest = new D7SendSmsRequest();
        smsRequest.setTo(detailFromDB.getPhoneNo());
        smsRequest.setFrom(Constant.SMS_FROM);
        smsRequest.setContent(otpMsg);
        HttpHeaders header = new HttpHeaders();
        header.add(otpApiKeyName, otpApiKeyValue);
        header.add(Header.X_CORRELATION_ID.getValue(), MDC.get(Constant.CORRELATION_ID));
        restExchangeUtil.execute(otpApi, HttpMethod.POST, header, smsRequest, D7SendSmsResponse.class);

        // Getting correlationId for response, for next request of login
        LoginCheckResponse checkResponse = new LoginCheckResponse();
        checkResponse.setReqId(uuid);
        ApiResponse response = new ApiResponse(ApplicationCode.REQUEST_PROCESSED_SUCCESSFULLY.getCode(),
                ApplicationCode.REQUEST_PROCESSED_SUCCESSFULLY.getMsg(), checkResponse);
        log.debug("Exiting LoginServiceImpl.processLoginCheck method");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * This will validate the LoginRequest
     * @param request {@link LoginRequest}
     * @throws InvalidPayloadException
     */
    private void validateLoginCheckRequest(LoginCheckRequest request) throws InvalidPayloadException {
        log.debug("Entering LoginServiceImpl.validateLoginCheckRequest method");
        if(!contactNumberValidator.isValid(request.getPhoneNo(), request.getCountryCode())){
            throw new InvalidPayloadException(ApplicationMessage.INVALID_PHONE_NUMBER_AND_COUNTRY_CODE_COMBINATION);
        }
        log.debug("Exiting LoginServiceImpl.validateLoginCheckRequest method");
    }

    /**
     * This will validate the LoginRequest
     * @param request {@link LoginRequest}
     * @throws InvalidPayloadException
     * @throws ConstraintViolationException
     */
    private void validateCreateConsumerRequest(LoginRequest request) throws InvalidPayloadException, ConstraintViolationException {
        log.debug("Entering LoginServiceImpl.validateCreateConsumerRequest method");
        if(!contactNumberValidator.isValid(request.getPhoneNo(), request.getCountryCode())){
            throw new InvalidPayloadException(ApplicationMessage.INVALID_PHONE_NUMBER_AND_COUNTRY_CODE_COMBINATION);
        }
        // Validate refId and OTP
        try{
            UUID.fromString(request.getRefId());
        }catch (IllegalArgumentException e){
            throw new InvalidPayloadException(ApplicationMessage.INVALID_REF_ID_AND_OTP_COMBINATION);
        }
        log.debug("Exiting LoginServiceImpl.validateCreateConsumerRequest method");
    }

    /**
     * This will return the User reference, based on phNo and xChannel
     * @param phNo {@link String}
     * @param xChannel {@link String}
     * @return {@link Base}
     * @throws InvalidXChannelException
     */
    private Base getUserDetail(String phNo, String xChannel) throws InvalidXChannelException {
        log.debug("Entering LoginServiceImpl.getUserDetail() method");
        Base detailFromDB = null;
        if (StringUtils.equalsIgnoreCase(XChannel.BUYER.getValue(), xChannel))
            detailFromDB = customerDetailDAO.findByPhoneNo(phNo);
        else if (StringUtils.equalsIgnoreCase(XChannel.SELLER.getValue(), xChannel))
            detailFromDB = restaurantDetailDAO.findByPhoneNo(phNo);
        else if (StringUtils.equalsIgnoreCase(XChannel.TRANSPORT.getValue(), xChannel))
            detailFromDB = deliveryPersonDetailDAO.findByPhoneNo(phNo);
        log.debug("Exiting LoginServiceImpl.getUserDetail() method");
        return detailFromDB;
    }

    /**
     * This will check, if customer phoneNo exists in db, and returns the token for the sames
     *
     * @param request {@link LoginRequest}
     * @return {@link ResponseEntity<ApiResponse>}
     * @throws Exception
     */
    private ResponseEntity<ApiResponse> getTokenResponse(LoginRequest request, String xChannel) throws Exception {
        log.debug("Entering LoginServiceImpl.handleCustomerDetailLoginRequest method");
        log.debug("Payload received : {}", request.toString());
        LoginResponse payload = new LoginResponse();
        Base detailFromDB = getUserDetail(request.getPhoneNo(), xChannel);
        if (null == detailFromDB || !detailFromDB.isActive()) {
            throw new InvalidPayloadException(ApplicationMessage.USER_NOT_FOUND_OR_INACTIVE);
        }
        if (StringUtils.isEmpty(detailFromDB.getToken())) {
            log.debug("Couldn't able to find the user token, creating new token");
            String token = jwtOperation.getTokenForUser(detailFromDB);
            Instant time = Instant.now();
            payload.setToken(token);
            payload.setCreatedAt(time);
            detailFromDB.setToken(token);
            detailFromDB.setTokenCreatedAt(time);
            detailFromDB.setUpdatedAt(time);
            if (detailFromDB instanceof CustomerDetail)
                customerDetailDAO.save((CustomerDetail) detailFromDB);
            else if (detailFromDB instanceof RestaurantDetail)
                restaurantDetailDAO.save((RestaurantDetail) detailFromDB);
            else
                deliveryPersonDetailDAO.save((DeliveryPersonDetail) detailFromDB);
        } else {
            payload.setToken(detailFromDB.getToken());
            payload.setCreatedAt(detailFromDB.getTokenCreatedAt());
        }
        ApiResponse result = new ApiResponse(ApplicationCode.REQUEST_PROCESSED_SUCCESSFULLY.getCode(),
                ApplicationCode.REQUEST_PROCESSED_SUCCESSFULLY.getMsg());
        result.setData(payload);
        log.debug("Exiting LoginServiceImpl.handleCustomerDetailLoginRequest method");
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}
