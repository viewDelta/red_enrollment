package com.red.ct.red_enrollment.sevice.impl;

import com.red.ct.data.dao.ICustomerDetailDAO;
import com.red.ct.data.dao.IDeliveryPersonDetailDAO;
import com.red.ct.data.dao.IRestaurantDetailDAO;
import com.red.ct.data.dao.IServiceLocationDAO;
import com.red.ct.data.model.CustomerDetail;
import com.red.ct.data.model.DeliveryPersonDetail;
import com.red.ct.data.model.RestaurantDetail;
import com.red.ct.data.model.ServiceLocation;
import com.red.ct.domain.inbound.request.CreateConsumerRequest;
import com.red.ct.domain.inbound.request.CreateDeliveryPersonRequest;
import com.red.ct.domain.inbound.request.CreateRestaurantRequest;
import com.red.ct.domain.inbound.request.IRequest;
import com.red.ct.domain.inbound.response.ApiResponse;
import com.red.ct.exception.DuplicateEntryException;
import com.red.ct.exception.InvalidDateFormatException;
import com.red.ct.exception.InvalidPayloadException;
import com.red.ct.red_enrollment.service.ISignUpService;
import com.red.ct.utility.HeaderValidation;
import com.red.ct.utility.ResponseHelper;
import com.red.ct.utility.constants.*;
import com.red.ct.utility.validator.ContactNumberValidator;
import com.red.ct.utility.validator.DateFormatValidator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.time.Instant;
import java.util.Set;
import java.util.UUID;

@Slf4j
@Service
public class SignUpServiceImpl implements ISignUpService {

    @Autowired
    private Validator validator;

    @Autowired
    private ModelMapper mapper;
    
    @Autowired
    private ICustomerDetailDAO customerDetailDAO;

    @Autowired
    private IDeliveryPersonDetailDAO deliveryPersonDetailDAO;

    @Autowired
    private IRestaurantDetailDAO restaurantDetailDAO;

    @Autowired
    private IServiceLocationDAO serviceLocationDAO;

    @Autowired
    private HeaderValidation headerValidation;

    @Autowired
    private ContactNumberValidator contactNumberValidator;

    @Override
    public ResponseEntity<ApiResponse> signUpForClient(CreateConsumerRequest request, HttpHeaders headers) throws Exception{
        log.debug("Entering SignUpServiceImpl.signUpForClient method");
        headerValidation.validateRequestHeader(headers, RequestType.SIGNUP_FOR_CONSUMER);
        validateCreateConsumerRequest(request);
        // validate if phone number is already present, then insert record
        persistCreateConsumerRequest(request);
        log.debug("Exiting SignUpServiceImpl.signUpForClient method");
        return ResponseHelper.buildSuccessResponse(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<ApiResponse> signUpForDeliveryPerson(CreateDeliveryPersonRequest request,  HttpHeaders headers) throws Exception{
        log.debug("Entering SignUpServiceImpl.signUpForDeliveryPerson method");
        headerValidation.validateRequestHeader(headers, RequestType.SIGNUP_FOR_DELIVERY_PERSON);
        validateCreateDeliveryPersonRequest(request);
        persistCreateDeliveryPersonRequest(request);
        log.debug("Exiting SignUpServiceImpl.signUpForDeliveryPerson method");
        return ResponseHelper.buildSuccessResponse(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<ApiResponse> signUpForRestaurant(CreateRestaurantRequest request, HttpHeaders headers) throws Exception {
        log.debug("Entering SignUpServiceImpl.signUpForRestaurant method");
        headerValidation.validateRequestHeader(headers, RequestType.SIGNUP_FOR_OUTLET);
        validateCreateRestaurantRequest(request);
        persistCreateRestaurantRequest(request);
        log.debug("Exiting SignUpServiceImpl.signUpForRestaurant method");
        return ResponseHelper.buildSuccessResponse(HttpStatus.CREATED);
    }

    /**
     * This will validate the createConsumerRequest
     * @param request {@link CreateConsumerRequest}
     * @throws Exception
     */
    private void validateCreateConsumerRequest(CreateConsumerRequest request) throws Exception{
        log.debug("Entering SignUpServiceImpl.validateCreateConsumerRequest method");
        validateConstraint(request);
        if(!contactNumberValidator.isValid(request.getPhoneNo(), request.getCountryCode())){
            throw new InvalidPayloadException(ApplicationMessage.INVALID_PHONE_NUMBER_AND_COUNTRY_CODE_COMBINATION);
        }
        if(StringUtils.isNotBlank(request.getDob()) && null == DateFormatValidator.getDateFromString(request.getDob())){
                throw new InvalidDateFormatException(ApplicationMessage.INVALID_OR_MISSING_DATE);
        }
        checkForServiceRegion(request.getCountryCode(), request.getStateCode(), request.getPinCode());
        log.debug("Exiting SignUpServiceImpl.validateCreateConsumerRequest method");
    }

    /**
     * This will validate the CreateDeliveryPersonRequest
     * @param request {@link CreateDeliveryPersonRequest}
     * @throws Exception
     */
    private void validateCreateDeliveryPersonRequest(CreateDeliveryPersonRequest request) throws Exception {
        log.debug("Entering SignUpServiceImpl.validateCreateDeliveryPersonRequest method");
        validateConstraint(request);
        if(!contactNumberValidator.isValid(request.getPhoneNo(), request.getCountryCode())){
            throw new InvalidPayloadException(ApplicationMessage.INVALID_PHONE_NUMBER_AND_COUNTRY_CODE_COMBINATION);
        }
        if(StringUtils.isNotBlank(request.getCountryCode()) && !CountryCode.isValidCode(request.getCountryCode())){
            throw new InvalidPayloadException("Invalid countryCode received");
        }
        checkForServiceRegion(request.getCountryCode(), request.getStateCode(), request.getPinCode());
        log.debug("Exiting SignUpServiceImpl.validateCreateConsumerRequest method");
    }

    /**
     * This will validate the CreateRestaurantRequest
     * @param request {@link CreateRestaurantRequest}
     * @throws Exception
     */
    private void validateCreateRestaurantRequest(CreateRestaurantRequest request) throws Exception {
        log.debug("Entering SignUpServiceImpl.validateCreateRestaurantRequest method");
        validateConstraint(request);
        if(!contactNumberValidator.isValid(request.getPhoneNo(), request.getCountryCode())){
            throw new InvalidPayloadException(ApplicationMessage.INVALID_PHONE_NUMBER_AND_COUNTRY_CODE_COMBINATION);
        }
        if(StringUtils.isNotBlank(request.getCountryCode()) && !CountryCode.isValidCode(request.getCountryCode())){
            throw new InvalidPayloadException("Invalid countryCode received");
        }
        checkForServiceRegion(request.getCountryCode(), request.getStateCode(), request.getPinCode());
        log.debug("Exiting SignUpServiceImpl.validateCreateRestaurantRequest method");
    }

    /**
     * This will validate constraint to a given request
     * @param request
     */
    private void validateConstraint(IRequest request){
        log.debug("Entering SignUpServiceImpl.validateConstraint method");
        //Validate bean
        Set<ConstraintViolation<IRequest>> constraintViolations = validator.validate(request);
        //Show errors
        if (!constraintViolations.isEmpty()) {
            throw new ConstraintViolationException(constraintViolations);
        }
        log.debug("Exiting SignUpServiceImpl.validateConstraint method");
    }

    /**
     * This will validate, if countryCode, stateCode and pinCode is servicable by our application
     * if, it's not serviceable then it throw InvalidPayloadException exception
     * @param countryCode {@link String}
     * @param stateCode {@link String}
     * @param pinCode {@link String}
     * @throws InvalidPayloadException {@link InvalidPayloadException}
     */
    private void checkForServiceRegion(String countryCode, String stateCode, String pinCode) throws InvalidPayloadException {
        if(CountryCode.isValidCode(countryCode) && StateCode.isValidCode(stateCode) && StringUtils.isNotBlank(pinCode)){
            ServiceLocation location = serviceLocationDAO.findByCountryCodeAndStateCode(countryCode, stateCode);
            if(null != location && location.getServicePinCodes().contains(StringUtils.trimToEmpty(pinCode))){
                return;
            }
        }
        throw new InvalidPayloadException("Invalid or unserviceable value of countryCode, stateCode or pinCode received");
    }

    /**
     * This will persist the entry to db, if phoneNumber is already registered, throws Invalid payload error
     * @param request {@link CreateConsumerRequest}
     * @throws DuplicateEntryException
     */
	private void persistCreateConsumerRequest(CreateConsumerRequest request) throws DuplicateEntryException {
		log.debug("Entering SignUpServiceImpl.persistCreateConsumerRequest method");
		try {
            CustomerDetail customerDetail = customerDetailDAO.findByPhoneNoOrEmail(request.getPhoneNo().trim(),
                    request.getEmail());
            if(null != customerDetail){
                throw new DuplicateEntryException(ApplicationMessage.INVALID_PAYLOAD);
            }
            customerDetail = mapper.map(request, CustomerDetail.class);
            Instant date = Instant.now();
            customerDetail.setUuid(UUID.randomUUID().toString());
            customerDetail.setCreatedAt(date);
            customerDetail.setUpdatedAt(date);
            customerDetail.setActive(true); // OTP based validation will be done from UI. Hence, we are assuming number is valid
            customerDetailDAO.save(customerDetail);
		} catch (IncorrectResultSizeDataAccessException | DuplicateEntryException e) {
			log.error("Multiple Entries found with phoneNo : {} and email : {}", request.getPhoneNo(), request.getEmail());
			throw new DuplicateEntryException(ApplicationCode.DUPLICATE_ENTRY_FOUND.getMsg());
		}
		log.debug("Exiting SignUpServiceImpl.persistCreateConsumerRequest method");
	}

    /**
     * This will persist the entry to db, if duplicate entry found, it will throw DuplicateEntry Exception
     * @param request {@link CreateDeliveryPersonRequest}
     * @throws DuplicateEntryException
     */
    private void persistCreateDeliveryPersonRequest(CreateDeliveryPersonRequest request) throws  DuplicateEntryException {
        log.debug("Entering SignUpServiceImpl.persistCreateDeliveryPersonRequest method");
        try {
            DeliveryPersonDetail deliveryPersonDetail =
                    deliveryPersonDetailDAO.findByPhoneNoOrEmailOrRCOrLicenceOrAadharNoOrPAN(request.getPhoneNo().trim(),
                            request.getEmail(), request.getRC(), request.getLicence(), request.getAadharNo(), request.getPAN());
            if (null != deliveryPersonDetail) {
                throw new DuplicateEntryException(ApplicationMessage.INVALID_PAYLOAD);
            }
            deliveryPersonDetail = mapper.map(request, DeliveryPersonDetail.class);
            Instant date = Instant.now();
            deliveryPersonDetail.setUuid(UUID.randomUUID().toString());
            deliveryPersonDetail.setCreatedAt(date);
            deliveryPersonDetail.setUpdatedAt(date);
            deliveryPersonDetail.setActive(false); // Licence will be verified and after, it will set as true.
            deliveryPersonDetailDAO.save(deliveryPersonDetail);
        } catch (IncorrectResultSizeDataAccessException | DuplicateEntryException e) {
            log.error("Multiple Entries found with phoneNo : {} or email : {} or pan : {} or rc : {} or licence : {}",
                    request.getPhoneNo(), request.getEmail(), request.getPAN(), request.getRC(), request.getLicence());
            throw new DuplicateEntryException(ApplicationCode.DUPLICATE_ENTRY_FOUND.getMsg());
        }
        log.debug("Exiting SignUpServiceImpl.persistCreateDeliveryPersonRequest method");
    }

    /**
     * This will persist the entry to db, if duplicate entry found, it will throw DuplicateEntry Exception
     * @param request {@link CreateRestaurantRequest}
     * @throws DuplicateEntryException
     */
    private void persistCreateRestaurantRequest(CreateRestaurantRequest request) throws DuplicateEntryException {
        log.debug("Entering SignUpServiceImpl.persistCreateRestaurantRequest method");
        try {
            RestaurantDetail restaurantDetail = restaurantDetailDAO.findByPhoneNoOrEmailOrPan(request.getPhoneNo().trim(),
                    request.getEmail(), request.getPan());
            if(null != restaurantDetail){
                throw new DuplicateEntryException(ApplicationMessage.INVALID_PAYLOAD);
            }
            restaurantDetail = mapper.map(request, RestaurantDetail.class);
            Instant date = Instant.now();
            restaurantDetail.setUuid(UUID.randomUUID().toString());
            restaurantDetail.setCreatedAt(date);
            restaurantDetail.setUpdatedAt(date);
            restaurantDetail.setActive(true);
            restaurantDetailDAO.save(restaurantDetail);
        } catch (IncorrectResultSizeDataAccessException | DuplicateEntryException e) {
            log.error("Multiple Entries found with phoneNo : {} or email : {} or pan : {}", request.getPhoneNo(), request.getEmail(), request.getPan());
            throw new DuplicateEntryException(ApplicationCode.DUPLICATE_ENTRY_FOUND.getMsg());
        }
        log.debug("Exiting SignUpServiceImpl.persistCreateRestaurantRequest method");
    }

}
