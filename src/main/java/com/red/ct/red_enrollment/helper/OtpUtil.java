package com.red.ct.red_enrollment.helper;

import com.google.crypto.tink.Aead;
import com.google.crypto.tink.KeysetHandle;
import com.google.crypto.tink.aead.AeadFactory;
import com.google.crypto.tink.aead.AeadKeyTemplates;
import com.google.crypto.tink.config.TinkConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.Base64;

@Slf4j
@Component
public class OtpUtil {

    private final Aead aead;

    public OtpUtil() throws GeneralSecurityException {
        // Initialize Tink configuration
        TinkConfig.register();
        // GENERATE key
        KeysetHandle keysetHandle = KeysetHandle.generateNew(AeadKeyTemplates.AES256_GCM);
        // ENCRYPTION
        aead = AeadFactory.getPrimitive(keysetHandle);
    }

    /**
     * Generates cipherText for mobile No w.r.t OTP
     * @param phoneNo {@link String}
     * @param otp {@link String}
     * @return {@link String}
     * @throws GeneralSecurityException
     */
    public String cipherForOTPWithPhoneNo(String phoneNo, String otp) throws GeneralSecurityException {
        log.debug("Entering CryptUtil.cipherForOTPWithPhoneNo() method");
        byte[] cipherTextBytes = aead.encrypt(phoneNo.getBytes(StandardCharsets.UTF_8), otp.getBytes());
        // conversion of raw bytes to BASE64 representation
        log.debug("Exiting CryptUtil.cipherForOTPWithPhoneNo() method");
        return new String(Base64.getEncoder().encode(cipherTextBytes));
    }

    /**
     * Returns valid mobile Number assoicated with the OTP
     * @param ciphertext {@link String}
     * @param otp {@link String}
     * @return {@link String}
     * @throws GeneralSecurityException
     */
    public String decryptOTPCipher(String ciphertext, String otp) throws GeneralSecurityException {
        log.debug("Entering CryptUtil.cipherForOTPWithPhoneNo() method");
        byte[] decryptedCipherTextBytes = aead.decrypt(Base64.getDecoder().decode(ciphertext), otp.getBytes());
        log.debug("Exiting CryptUtil.cipherForOTPWithPhoneNo() method");
        return new String(decryptedCipherTextBytes, StandardCharsets.UTF_8);
    }
}
