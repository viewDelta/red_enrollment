package com.red.ct.red_enrollment.controller.advice;

import com.red.ct.domain.inbound.response.ApiResponse;
import com.red.ct.exception.*;
import com.red.ct.utility.ResponseHelper;
import com.red.ct.utility.constants.ApplicationCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<ApiResponse> handleException(ConstraintViolationException ex){
		log.error("ConstraintViolationException {}", ex.getMessage());
		ApiResponse payload = new ApiResponse(ApplicationCode.INVALID_PAYLOAD.getCode(), 
    			ApplicationCode.INVALID_PAYLOAD.getMsg());
    	List<ApiResponse> errorList = new ArrayList<ApiResponse>();
		ex.getConstraintViolations().
				forEach(violation -> errorList.add(new ApiResponse(ResponseHelper.getCodeFromMsg(violation.getMessage()), violation.getMessage())));
    	payload.setErrors(errorList);
    	return new ResponseEntity<>(payload, HttpStatus.BAD_REQUEST);
    }
	
	@ExceptionHandler({InvalidDateFormatException.class, DuplicateEntryException.class})
	public ResponseEntity<ApiResponse> handleDateFormatOrDuplicateEntryException(Exception ex){
		log.error("DateFormatOrDuplicateEntryException : {}", ex.getMessage());
		ApiResponse payload = new ApiResponse(ApplicationCode.INVALID_PAYLOAD.getCode(),
    			ApplicationCode.INVALID_PAYLOAD.getMsg());
    	List<ApiResponse> errorList = new ArrayList<ApiResponse>();
    	errorList.add(new ApiResponse(ResponseHelper.getCodeFromMsg(ex.getMessage()), ex.getMessage()));
        payload.setErrors(errorList);
    	return new ResponseEntity<>(payload, HttpStatus.BAD_REQUEST);
   }

	@ExceptionHandler({InvalidXChannelException.class, InvalidHeaderValueException.class})
	public ResponseEntity<ApiResponse> handleInvalidHeaderValueException(Exception ex) {
		log.error("InvalidHeaderValueException : {}", ex.getMessage());
		ApiResponse payload = new ApiResponse(ApplicationCode.INVALID_OR_MISSING_API_KEY_OR_X_CHANNEL.getCode(),
				ex.getMessage());
		return new ResponseEntity<>(payload, HttpStatus.FORBIDDEN);
	}

    @ExceptionHandler(InvalidPayloadException.class)
    public ResponseEntity<ApiResponse> handleException(InvalidPayloadException ex){
    	log.error("Invalid Payload received : {}", ex.getMessage());
    	ApiResponse payload = new ApiResponse(ApplicationCode.INVALID_PAYLOAD.getCode(), 
    			ApplicationCode.INVALID_PAYLOAD.getMsg());
    	List<ApiResponse> errorList = new ArrayList<ApiResponse>();
    	errorList.add(new ApiResponse(ApplicationCode.INVALID_PAYLOAD.getCode(), ex.getMessage()));
    	payload.setErrors(errorList);
    	return new ResponseEntity<>(payload, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({Exception.class, GeneralSecurityException.class})
    public ResponseEntity<ApiResponse> handleException(Exception ex){
    	log.error("Exception occured : {}", ex);
        ApiResponse error = new ApiResponse(ApplicationCode.INTERNAL_SERVER_ERROR.getCode(), 
        		ApplicationCode.INTERNAL_SERVER_ERROR.getMsg());
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
