package com.red.ct.red_enrollment.controller;

import com.red.ct.domain.inbound.request.CreateConsumerRequest;
import com.red.ct.domain.inbound.request.CreateDeliveryPersonRequest;
import com.red.ct.domain.inbound.request.CreateRestaurantRequest;
import com.red.ct.domain.inbound.response.ApiResponse;
import com.red.ct.red_enrollment.service.ISignUpService;
import com.red.ct.utility.constants.EndPoint;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(EndPoint.SIGNUP)
public class SignUpController {

	@Autowired
	private ISignUpService signUpService;
	
	@PostMapping(path = EndPoint.CLIENT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ApiResponse> postConsumer(@RequestBody CreateConsumerRequest createConsumerRequest, @RequestHeader HttpHeaders headers) throws Exception{
		log.debug("Entering SignUpController.postConsumer method");
		ResponseEntity<ApiResponse> response = signUpService.signUpForClient(createConsumerRequest, headers);
		log.debug("Exiting SignUpController.postConsumer method");
		return response;
    }

	@PostMapping(path = EndPoint.TRANSPORT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> postDeliveryPerson(@RequestBody CreateDeliveryPersonRequest createDeliveryPersonRequest, @RequestHeader HttpHeaders headers) throws Exception{
		log.debug("Entering SignUpController.postDeliveryPerson method");
		ResponseEntity<ApiResponse> response = signUpService.signUpForDeliveryPerson(createDeliveryPersonRequest, headers);
		log.debug("Exiting SignUpController.postDeliveryPerson method");
		return response;
	}

	@PostMapping(path = EndPoint.OUTLET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> postRestaurant(@RequestBody CreateRestaurantRequest createRestaurantRequest, @RequestHeader HttpHeaders headers) throws Exception{
		log.debug("Entering SignUpController.postRestaurant method");
		ResponseEntity<ApiResponse> response = signUpService.signUpForRestaurant(createRestaurantRequest, headers);
		log.debug("Exiting SignUpController.postRestaurant method");
		return response;
	}

}
