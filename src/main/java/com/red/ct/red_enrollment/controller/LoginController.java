package com.red.ct.red_enrollment.controller;

import com.red.ct.domain.inbound.request.LoginCheckRequest;
import com.red.ct.domain.inbound.request.LoginRequest;
import com.red.ct.domain.inbound.response.ApiResponse;
import com.red.ct.red_enrollment.service.ILoginService;
import com.red.ct.utility.constants.EndPoint;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class LoginController {
	
	@Autowired
	private ILoginService loginService;

	@PostMapping(path = EndPoint.LOGIN, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ApiResponse> login(@RequestBody LoginRequest loginRequest, @RequestHeader HttpHeaders headers) throws Exception{
		log.debug("Entering LoginController.login method");
		ResponseEntity<ApiResponse> response = loginService.processLogin(loginRequest, headers);
		log.debug("Exiting LoginController.login method");
		return response;
    }

	@PostMapping(path = EndPoint.LOGIN_CHECK, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> loginCheck(@RequestBody LoginCheckRequest request, @RequestHeader HttpHeaders headers) throws Exception{
		log.debug("Entering LoginController.loginCheck method");
		ResponseEntity<ApiResponse> response = loginService.processLoginCheck(request, headers);
		log.debug("Exiting LoginController.loginCheck method");
		return response;
	}
}
