package com.red.ct.red_enrollment;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

@Slf4j
@EnableAutoConfiguration
@EnableConfigurationProperties
@SpringBootApplication
@EnableCaching
@EnableAsync
@ComponentScan("com.red.ct")
public class RedEnrollmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(RedEnrollmentApplication.class, args);
		log.info("Enrollment service started successfully");
	}
}
