package com.red.ct.red_enrollment.service;

import com.red.ct.domain.inbound.request.LoginCheckRequest;
import com.red.ct.domain.inbound.request.LoginRequest;
import com.red.ct.domain.inbound.response.ApiResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

public interface ILoginService {

    /**
     * This will process the login request, w.r.t to OTP and return token
     * @param loginRequest {@link LoginRequest}
     * @param headers {@link HttpHeaders}
     * @return {@link ResponseEntity<>}
     * @throws Exception
     */
	ResponseEntity<ApiResponse> processLogin(LoginRequest loginRequest, HttpHeaders headers) throws Exception;

    /**
     * This will trigger the OTP requeest for the given mobile number, if number exists in the database
     * @param request {@link LoginCheckRequest}
     * @param headers {@link HttpHeaders}
     * @return {@link ResponseEntity<>}
     * @throws Exception
     */
    ResponseEntity<ApiResponse> processLoginCheck(LoginCheckRequest request, HttpHeaders headers) throws Exception;
}
