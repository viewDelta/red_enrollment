package com.red.ct.red_enrollment.service;

import com.red.ct.domain.inbound.request.CreateConsumerRequest;
import com.red.ct.domain.inbound.request.CreateDeliveryPersonRequest;
import com.red.ct.domain.inbound.request.CreateRestaurantRequest;
import com.red.ct.domain.inbound.response.ApiResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

public interface ISignUpService {
    ResponseEntity<ApiResponse> signUpForClient(CreateConsumerRequest request, HttpHeaders headers) throws Exception;
    ResponseEntity<ApiResponse> signUpForDeliveryPerson(CreateDeliveryPersonRequest request, HttpHeaders headers) throws Exception;
    ResponseEntity<ApiResponse> signUpForRestaurant(CreateRestaurantRequest request, HttpHeaders headers) throws Exception;
}
