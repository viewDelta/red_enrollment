package com.red.ct.red_enrollment.sevice.impl;

import com.red.ct.data.dao.ICustomerDetailDAO;
import com.red.ct.data.dao.IDeliveryPersonDetailDAO;
import com.red.ct.data.dao.IRestaurantDetailDAO;
import com.red.ct.data.dao.IServiceLocationDAO;
import com.red.ct.data.model.CustomerDetail;
import com.red.ct.data.model.DeliveryPersonDetail;
import com.red.ct.data.model.RestaurantDetail;
import com.red.ct.data.model.ServiceLocation;
import com.red.ct.domain.inbound.request.CreateConsumerRequest;
import com.red.ct.domain.inbound.request.CreateDeliveryPersonRequest;
import com.red.ct.domain.inbound.request.CreateRestaurantRequest;
import com.red.ct.domain.inbound.request.Location;
import com.red.ct.exception.DuplicateEntryException;
import com.red.ct.exception.InvalidDateFormatException;
import com.red.ct.exception.InvalidPayloadException;
import com.red.ct.utility.HeaderValidation;
import com.red.ct.utility.validator.ContactNumberValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class SignUpServiceImplTest {

    @Mock
    private Validator validator;

    @Mock
    private ModelMapper mapper;

    @Mock
    private ICustomerDetailDAO customerDetailDAO;

    @Mock
    private IDeliveryPersonDetailDAO deliveryPersonDetailDAO;

    @Mock
    private IRestaurantDetailDAO restaurantDetailDAO;

    @Mock
    private IServiceLocationDAO serviceLocationDAO;

    @InjectMocks
    private SignUpServiceImpl signUpServiceImpl;

    @Mock
    private HeaderValidation headerValidation;

    @Mock
    private ContactNumberValidator contactNumberValidator;

    private CreateConsumerRequest createConsumerRequest = new CreateConsumerRequest();
    private CustomerDetail customerDetailMapper = new CustomerDetail();
    Set<ConstraintViolation<CreateConsumerRequest>> constraintViolations = new HashSet<>();


    private CreateDeliveryPersonRequest createDeliveryPersonRequest = new CreateDeliveryPersonRequest();
    private DeliveryPersonDetail deliveryPersonDetailMapper = new DeliveryPersonDetail();
    Set<ConstraintViolation<CreateDeliveryPersonRequest>> constraintViolationsForDeliveryPerson = new HashSet<>();

    private CreateRestaurantRequest createRestaurantRequest = new CreateRestaurantRequest();
    private RestaurantDetail restaurantDetailMapper = new RestaurantDetail();
    Set<ConstraintViolation<CreateRestaurantRequest>> constraintViolationsForRestaurant = new HashSet<>();


    @BeforeEach
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);
        createConsumerRequest.setCountryCode("IN");
        createConsumerRequest.setEmail("abc@test.com");
        createConsumerRequest.setPhoneNo("8909876890");
        customerDetailMapper.setCountryCode("IN");
        customerDetailMapper.setPhoneNo("8909876890");
        customerDetailMapper.setEmail("abc@test.com");
        createConsumerRequest.setStateCode("OD");
        createConsumerRequest.setPinCode("765017");

        createDeliveryPersonRequest.setAadharNo("444433332222");
        createDeliveryPersonRequest.setPAN("CHBQO0192U");
        createDeliveryPersonRequest.setRC("OD 01 AA 1111");
        createDeliveryPersonRequest.setLicence("OD-0619850031234");
        createDeliveryPersonRequest.setPhoneNo("8909877890");
        createDeliveryPersonRequest.setEmail("abc@test.com");
        createDeliveryPersonRequest.setName("Abc");
        deliveryPersonDetailMapper.setPhoneNo("8909877890");
        deliveryPersonDetailMapper.setEmail("abc@test.com");
        deliveryPersonDetailMapper.setCountryCode("IN");
        deliveryPersonDetailMapper.setLicence("OD-0619850031234");
        deliveryPersonDetailMapper.setRC("OD 01 AA 1111");
        createDeliveryPersonRequest.setCountryCode("IN");
        createDeliveryPersonRequest.setStateCode("OD");
        createDeliveryPersonRequest.setPinCode("765017");

        createRestaurantRequest.setRestaurantName("Abc Restaurant");
        Location restaurantLocation = new Location();
        restaurantLocation.setLatitude(37.758056);
        restaurantLocation.setLongitude(-122.425332);
        createRestaurantRequest.setLocation(restaurantLocation);
        createRestaurantRequest.setPhoneNo("9854125412");
        createRestaurantRequest.setName("Ramesh Tyagi");
        createRestaurantRequest.setEmail("abc@email.com");
        createRestaurantRequest.setPan("CHBQO0192U");
        restaurantDetailMapper.setPhoneNo("9854125412");
        restaurantDetailMapper.setEmail("abc@email.com");
        restaurantDetailMapper.setCountryCode("IN");
        restaurantDetailMapper.setPan("CHBQO0192U");
        createRestaurantRequest.setStateCode("OD");
        createRestaurantRequest.setCountryCode("IN");
        createRestaurantRequest.setPinCode("765017");
        doNothing().when(headerValidation).validateRequestHeader(any(), any());
        doReturn(true).when(contactNumberValidator).isValid(anyString(), anyString());

    }

    @Test
    public void signUpForClientTest_forSuccess() throws Exception {
        ServiceLocation location = new ServiceLocation();
        location.setCountryCode("IN");
        location.setStateCode("OR");
        List<String> servicePinCode = new ArrayList<>();
        servicePinCode.add("765017");
        location.setServicePinCodes(servicePinCode);
        when(serviceLocationDAO.findByCountryCodeAndStateCode(anyString(), anyString())).thenReturn(location);
        when(customerDetailDAO.findByPhoneNoOrEmail(anyString(), anyString())).thenReturn(null);
        when(customerDetailDAO.save(any(CustomerDetail.class))).thenReturn(new CustomerDetail());
        when(validator.validate(any(CreateConsumerRequest.class))).thenReturn(constraintViolations);
        when(mapper.map(createConsumerRequest, CustomerDetail.class)).thenReturn(customerDetailMapper);
        assertEquals(HttpStatus.CREATED,
                signUpServiceImpl.signUpForClient(createConsumerRequest, new HttpHeaders()).getStatusCode());
    }

    @Test
    public void test_signUpForClient_withInvalidPhonenumberAndCountryCode() throws Exception {
        createConsumerRequest.setPhoneNo("+918090765456");
        createConsumerRequest.setCountryCode("US");
        ServiceLocation location = new ServiceLocation();
        location.setCountryCode("IN");
        location.setStateCode("OR");
        List<String> servicePinCode = new ArrayList<>();
        servicePinCode.add("765017");
        location.setServicePinCodes(servicePinCode);
        when(validator.validate(any(CreateConsumerRequest.class))).thenReturn(constraintViolations);
        when(mapper.map(createConsumerRequest, CustomerDetail.class)).thenReturn(customerDetailMapper);
        doReturn(false).when(contactNumberValidator).isValid(anyString(), anyString());
        assertThrows(InvalidPayloadException.class, () -> {
            signUpServiceImpl.signUpForClient(createConsumerRequest, new HttpHeaders());
        });
    }

    @Test
    public void signUpForClientTest_forInvalidPinCode_shouldThrowException() throws Exception {
        ServiceLocation location = new ServiceLocation();
        location.setCountryCode("IN");
        location.setStateCode("KA");
        List<String> servicePinCode = new ArrayList<>();
        servicePinCode.add("560063");
        location.setServicePinCodes(servicePinCode);
        when(serviceLocationDAO.findByCountryCodeAndStateCode(anyString(), anyString())).thenReturn(location);
        when(customerDetailDAO.findByPhoneNoOrEmail(anyString(), anyString())).thenReturn(null);
        when(customerDetailDAO.save(any(CustomerDetail.class))).thenReturn(new CustomerDetail());
        when(validator.validate(any(CreateConsumerRequest.class))).thenReturn(constraintViolations);
        when(mapper.map(createConsumerRequest, CustomerDetail.class)).thenReturn(customerDetailMapper);
        assertThrows(InvalidPayloadException.class, () -> {
                signUpServiceImpl.signUpForClient(createConsumerRequest, new HttpHeaders()).getStatusCode();
        });
    }

    @Test
    public void signUpForDeliveryPersonTest_forSuccess() throws Exception {
        ServiceLocation location = new ServiceLocation();
        location.setCountryCode("IN");
        location.setStateCode("OR");
        List<String> servicePinCode = new ArrayList<>();
        servicePinCode.add("765017");
        location.setServicePinCodes(servicePinCode);
        when(serviceLocationDAO.findByCountryCodeAndStateCode(anyString(), anyString())).thenReturn(location);
        when(deliveryPersonDetailDAO.findByPhoneNo(anyString())).thenReturn(null);
        when(deliveryPersonDetailDAO.save(any(DeliveryPersonDetail.class))).thenReturn(new DeliveryPersonDetail());
        when(validator.validate(any(CreateDeliveryPersonRequest.class))).thenReturn(constraintViolationsForDeliveryPerson);
        when(mapper.map(createDeliveryPersonRequest, DeliveryPersonDetail.class)).thenReturn(deliveryPersonDetailMapper);
        assertEquals(HttpStatus.CREATED,
                signUpServiceImpl.signUpForDeliveryPerson(createDeliveryPersonRequest, new HttpHeaders()).getStatusCode());
    }

    @Test
    public void testSignUpForDeliveryPersonTest_withInvalidPhonenumberAndCountryCode() throws Exception {
        createDeliveryPersonRequest.setPhoneNo("+918090765456");
        createDeliveryPersonRequest.setCountryCode("US");
        ServiceLocation location = new ServiceLocation();
        location.setCountryCode("IN");
        location.setStateCode("OR");
        List<String> servicePinCode = new ArrayList<>();
        servicePinCode.add("765017");
        location.setServicePinCodes(servicePinCode);
        when(validator.validate(any(CreateDeliveryPersonRequest.class))).thenReturn(constraintViolationsForDeliveryPerson);
        when(mapper.map(createDeliveryPersonRequest, DeliveryPersonDetail.class)).thenReturn(deliveryPersonDetailMapper);
        doReturn(false).when(contactNumberValidator).isValid(anyString(), anyString());
        assertThrows(InvalidPayloadException.class, () -> {
                signUpServiceImpl.signUpForDeliveryPerson(createDeliveryPersonRequest, new HttpHeaders());
        });
    }

    @Test
    public void signUpForRestaurantTest_forSuccess() throws Exception
    {
        ServiceLocation location = new ServiceLocation();
        location.setCountryCode("IN");
        location.setStateCode("OR");
        List<String> servicePinCode = new ArrayList<>();
        servicePinCode.add("765017");
        location.setServicePinCodes(servicePinCode);
        when(serviceLocationDAO.findByCountryCodeAndStateCode(anyString(), anyString())).thenReturn(location);
        when(restaurantDetailDAO.findByPhoneNo(anyString())).thenReturn(null);
        when(restaurantDetailDAO.save(any(RestaurantDetail.class))).thenReturn(new RestaurantDetail());
        when(validator.validate(any(CreateRestaurantRequest.class))).thenReturn(constraintViolationsForRestaurant);
        when(mapper.map(createRestaurantRequest, RestaurantDetail.class)).thenReturn(restaurantDetailMapper);
        assertEquals(HttpStatus.CREATED,
                signUpServiceImpl.signUpForRestaurant(createRestaurantRequest, new HttpHeaders()).getStatusCode());
    }

    @Test
    public void test_signUpForRestaurantTest_withInvalidPhonenumberAndCountryCode() throws Exception
    {
        createRestaurantRequest.setPhoneNo("+918090765456");
        createRestaurantRequest.setCountryCode("US");
        ServiceLocation location = new ServiceLocation();
        location.setCountryCode("IN");
        location.setStateCode("OR");
        List<String> servicePinCode = new ArrayList<>();
        servicePinCode.add("765017");
        location.setServicePinCodes(servicePinCode);
        when(validator.validate(any(CreateRestaurantRequest.class))).thenReturn(constraintViolationsForRestaurant);
        when(mapper.map(createRestaurantRequest, RestaurantDetail.class)).thenReturn(restaurantDetailMapper);
        assertThrows(InvalidPayloadException.class, () -> {
            signUpServiceImpl.signUpForRestaurant(createRestaurantRequest, new HttpHeaders());
        });
    }

    @Test
    public void signUpForClientTest_shouldThrowException_ifMobileAlreadyPresent_inDB() throws Exception {
        ServiceLocation location = new ServiceLocation();
        location.setCountryCode("IN");
        location.setStateCode("OR");
        List<String> servicePinCode = new ArrayList<>();
        servicePinCode.add("765017");
        location.setServicePinCodes(servicePinCode);
        when(serviceLocationDAO.findByCountryCodeAndStateCode(anyString(), anyString())).thenReturn(location);
        when(validator.validate(any(CreateConsumerRequest.class))).thenReturn(constraintViolations);
        when(mapper.map(createConsumerRequest, CustomerDetail.class)).thenReturn(customerDetailMapper);
        when(customerDetailDAO.findByPhoneNoOrEmail(anyString(), anyString())).thenReturn(customerDetailMapper);
        assertThrows(DuplicateEntryException.class, () -> {
            signUpServiceImpl.signUpForClient(createConsumerRequest, new HttpHeaders());
        });
    }

    @Test
    public void signUpForDeliveryPersonTest_shouldThrowException_ifMobileAlreadyPresent_inDB() throws Exception {
        ServiceLocation location = new ServiceLocation();
        location.setCountryCode("IN");
        location.setStateCode("OR");
        List<String> servicePinCode = new ArrayList<>();
        servicePinCode.add("765017");
        location.setServicePinCodes(servicePinCode);
        when(serviceLocationDAO.findByCountryCodeAndStateCode(anyString(), anyString())).thenReturn(location);
        when(validator.validate(any(CreateDeliveryPersonRequest.class))).thenReturn(constraintViolationsForDeliveryPerson);
        when(mapper.map(createDeliveryPersonRequest, DeliveryPersonDetail.class)).thenReturn(deliveryPersonDetailMapper);
        when(deliveryPersonDetailDAO.findByPhoneNoOrEmailOrRCOrLicenceOrAadharNoOrPAN(anyString(),
                anyString(), anyString(), anyString(), anyString(), anyString())).thenReturn(deliveryPersonDetailMapper);
        assertThrows(DuplicateEntryException.class, () -> {
            signUpServiceImpl.signUpForDeliveryPerson(createDeliveryPersonRequest, new HttpHeaders());
        });
    }

    @Test
    public void signUpForRestaurantTest_shouldThrowException_ifMobileAlreadyPresent_inDB() throws Exception {
        ServiceLocation location = new ServiceLocation();
        location.setCountryCode("IN");
        location.setStateCode("OR");
        List<String> servicePinCode = new ArrayList<>();
        servicePinCode.add("765017");
        location.setServicePinCodes(servicePinCode);
        when(serviceLocationDAO.findByCountryCodeAndStateCode(anyString(), anyString())).thenReturn(location);
        when(validator.validate(any(CreateRestaurantRequest.class))).thenReturn(constraintViolationsForRestaurant);
        when(mapper.map(createRestaurantRequest, RestaurantDetail.class)).thenReturn(restaurantDetailMapper);
        when(restaurantDetailDAO.findByPhoneNoOrEmailOrPan(anyString(), anyString(), anyString())).thenReturn(restaurantDetailMapper);
        assertThrows(DuplicateEntryException.class, () -> {
            signUpServiceImpl.signUpForRestaurant(createRestaurantRequest, new HttpHeaders());
        });
    }


    @Test
    public void signUpForClientTest_shouldThrowException_ifEmailIsLinked_toDifferentAccount_inDB() throws Exception {
        ServiceLocation location = new ServiceLocation();
        location.setCountryCode("IN");
        location.setStateCode("OR");
        List<String> servicePinCode = new ArrayList<>();
        servicePinCode.add("765017");
        location.setServicePinCodes(servicePinCode);
        when(serviceLocationDAO.findByCountryCodeAndStateCode(anyString(), anyString())).thenReturn(location);
        when(validator.validate(any(CreateConsumerRequest.class)) ).thenReturn(constraintViolations);
        when(mapper.map(createConsumerRequest, CustomerDetail.class)).thenReturn(customerDetailMapper);
        when(customerDetailDAO.findByPhoneNoOrEmail(anyString(), anyString()))
                .thenThrow(new IncorrectResultSizeDataAccessException(1));
        assertThrows(DuplicateEntryException.class, () -> {
            signUpServiceImpl.signUpForClient(createConsumerRequest, new HttpHeaders());
        });
    }

    @Test
    public void signUpForClientTest_forInvalidPayload_failedInConstraintViolation() throws Exception {
        constraintViolations.add(mock(ConstraintViolation.class));
        when(validator.validate(any(CreateConsumerRequest.class))).thenReturn(constraintViolations);
        assertThrows(ConstraintViolationException.class, () -> {
             signUpServiceImpl.signUpForClient(createConsumerRequest, new HttpHeaders());
        });
    }

    @Test
    public void signUpForDeliveryPersonTest_forInvalidPayload_failedInConstraintViolation() throws Exception {
        ConstraintViolation violation = mock(ConstraintViolation.class);
        constraintViolationsForDeliveryPerson.add(violation);
        when(validator.validate(any(CreateDeliveryPersonRequest.class))).thenReturn(constraintViolationsForDeliveryPerson);
        assertThrows(ConstraintViolationException.class, () -> {
            signUpServiceImpl.signUpForDeliveryPerson(createDeliveryPersonRequest, new HttpHeaders());
        });
    }

    @Test
    public void signUpForRestaurantTest_forInvalidPayload_failedInConstraintViolation() throws Exception {
        ConstraintViolation violation = mock(ConstraintViolation.class);
        constraintViolationsForRestaurant.add(violation);
        when(validator.validate(any(CreateRestaurantRequest.class))).thenReturn(constraintViolationsForRestaurant);
        assertThrows(ConstraintViolationException.class, () -> {
            signUpServiceImpl.signUpForRestaurant(createRestaurantRequest, new HttpHeaders());
        });
    }


    @Test
    public void signUpForClientTest_forInvalidPayload_failedInvalidDob() throws Exception {
        createConsumerRequest.setDob("ABC_TEST");
        when(validator.validate(any(CreateConsumerRequest.class))).thenReturn(constraintViolations);
        assertThrows(InvalidDateFormatException.class, () -> {
            signUpServiceImpl.signUpForClient(createConsumerRequest, new HttpHeaders());
        });
    }

    @Test
    public void signUpForClientTest_forInvalidPayload_failedInvalidCountryCode() throws Exception {
        createConsumerRequest.setCountryCode("LM");
        when(validator.validate(any(CreateConsumerRequest.class))).thenReturn(constraintViolations);
        assertThrows(InvalidPayloadException.class, () -> {
            signUpServiceImpl.signUpForClient(createConsumerRequest, new HttpHeaders());
        });
    }

    @Test
    public void signUpForDeliveryPersonTest_forInvalidPayload_failedInvalidCountryCode() throws Exception {
        createDeliveryPersonRequest.setCountryCode("LM");
        when(validator.validate(any(CreateDeliveryPersonRequest.class))).thenReturn(constraintViolationsForDeliveryPerson);
        assertThrows(InvalidPayloadException.class, () -> {
            signUpServiceImpl.signUpForDeliveryPerson(createDeliveryPersonRequest, new HttpHeaders());
        });
    }

    @Test
    public void signUpForRestaurantTest_forInvalidPayload_failedInvalidCountryCode() throws Exception {
        createRestaurantRequest.setCountryCode("LM");
        when(validator.validate(any(CreateRestaurantRequest.class))).thenReturn(constraintViolationsForRestaurant);
        assertThrows(InvalidPayloadException.class, () -> {
            signUpServiceImpl.signUpForRestaurant(createRestaurantRequest, new HttpHeaders());
        });
    }
}