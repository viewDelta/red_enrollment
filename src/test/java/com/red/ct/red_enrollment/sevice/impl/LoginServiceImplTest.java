package com.red.ct.red_enrollment.sevice.impl;

import com.red.ct.data.dao.ICustomerDetailDAO;
import com.red.ct.data.dao.IDeliveryPersonDetailDAO;
import com.red.ct.data.dao.IRestaurantDetailDAO;
import com.red.ct.data.model.CustomerDetail;
import com.red.ct.data.model.DeliveryPersonDetail;
import com.red.ct.data.model.RestaurantDetail;
import com.red.ct.domain.inbound.request.LoginCheckRequest;
import com.red.ct.domain.inbound.request.LoginRequest;
import com.red.ct.exception.InvalidHeaderValueException;
import com.red.ct.exception.InvalidPayloadException;
import com.red.ct.red_enrollment.helper.OtpUtil;
import com.red.ct.utility.HeaderValidation;
import com.red.ct.utility.JwtOperation;
import com.red.ct.utility.RestExchangeUtil;
import com.red.ct.utility.constants.ApplicationMessage;
import com.red.ct.utility.constants.Header;
import com.red.ct.utility.constants.XChannel;
import com.red.ct.utility.validator.ContactNumberValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.util.ReflectionTestUtils;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LoginServiceImplTest {

    @Mock
    private Validator validator;

    @Mock
    private ICustomerDetailDAO customerDetailDAO;

    @Mock
    private IRestaurantDetailDAO restaurantDetailDAO;

    @Mock
    private IDeliveryPersonDetailDAO deliveryPersonDetailDAO;

    @Mock
    private JwtOperation jwtOperation;

    @InjectMocks
    private LoginServiceImpl loginServiceImpl;

    @Mock
    private HeaderValidation headerValidation;

    @Mock
    private RedisTemplate<String, Object> redisTemplate;

    @Mock
    private RestExchangeUtil restExchange;

    @Mock
    private OtpUtil otpUtil;

    @Mock
    private ContactNumberValidator contactNumberValidator;

    @Mock
    private RestExchangeUtil restExchangeUtil;

    private CustomerDetail customerDetail = new CustomerDetail();
    private RestaurantDetail restaurantDetail = new RestaurantDetail();
    private DeliveryPersonDetail deliveryPersonDetail = new DeliveryPersonDetail();
    private LoginRequest request = new LoginRequest();
    private HttpHeaders headers = new HttpHeaders();
    Set<ConstraintViolation<LoginRequest>> constraintViolations = new HashSet<>();

    @Before
    public void init() throws Exception {
        String token = "token";
        customerDetail.setToken(token);
        customerDetail.setActive(true);
        customerDetail.setCreatedAt(Instant.now());
        restaurantDetail.setActive(true);
        restaurantDetail.setToken(token);
        restaurantDetail.setCreatedAt(Instant.now());
        deliveryPersonDetail.setToken(token);
        deliveryPersonDetail.setActive(true);
        deliveryPersonDetail.setCreatedAt(Instant.now());
        when(customerDetailDAO.findByPhoneNo(anyString())).thenReturn(customerDetail);
        when(restaurantDetailDAO.findByPhoneNo(anyString())).thenReturn(restaurantDetail);
        when(deliveryPersonDetailDAO.findByPhoneNo(anyString())).thenReturn(deliveryPersonDetail);
        when(customerDetailDAO.save(any(CustomerDetail.class))).thenReturn(customerDetail);
        when(restaurantDetailDAO.save(any(RestaurantDetail.class))).thenReturn(restaurantDetail);
        when(deliveryPersonDetailDAO.save(any(DeliveryPersonDetail.class))).thenReturn(deliveryPersonDetail);
        when(jwtOperation.getTokenForUser(any())).thenReturn(token);
        doNothing().when(headerValidation).validateRequestHeader(any(), any());
        ReflectionTestUtils.setField(loginServiceImpl, "otpApiKeyName", "otpApiKeyName");
        ReflectionTestUtils.setField(loginServiceImpl, "otpApiKeyValue", "otpApiKeyValue");
    }

    @Test
    public void processLoginTest_whenValidLoginRequest() throws Exception {
        request.setPhoneNo("+918092212312");
        request.setOTP("0911");
        request.setRefId("c9891bf9-e1ec-446e-aa6e-ebe384cf6849");
        request.setCountryCode("IN");
        ValueOperations ops = mock(ValueOperations.class);
        when(ops.get(anyString())).thenReturn("zfssawaw1seses5556");
        when(redisTemplate.opsForValue()).thenReturn(ops);
        when(redisTemplate.delete(anyString())).thenReturn(true);
        when(otpUtil.decryptOTPCipher(anyString(), anyString())).thenReturn("+918092212312");
        when(contactNumberValidator.isValid(anyString(), anyString())).thenReturn(true);
        headers.add(Header.X_CHANNEL.getValue(), XChannel.BUYER.getValue());
        assertEquals(HttpStatus.OK, loginServiceImpl.processLogin(request, headers).getStatusCode());
        headers.remove(Header.X_CHANNEL.getValue());
        headers.add(Header.X_CHANNEL.getValue(), XChannel.SELLER.getValue());
        assertEquals(HttpStatus.OK, loginServiceImpl.processLogin(request, headers).getStatusCode());
        headers.remove(Header.X_CHANNEL.getValue());
        headers.add(Header.X_CHANNEL.getValue(), XChannel.TRANSPORT.getValue());
        assertEquals(HttpStatus.OK, loginServiceImpl.processLogin(request, headers).getStatusCode());
    }

    @Test(expected = InvalidPayloadException.class)
    public void processLoginTest_whenCipherisInvalid() throws Exception {
        request.setPhoneNo("+918092212312");
        request.setOTP("0911");
        request.setRefId("c9891bf9-e1ec-446e-aa6e-ebe384cf6849");
        request.setCountryCode("IN");
        ValueOperations ops = mock(ValueOperations.class);
        when(ops.get(anyString())).thenReturn("zfssawaw1seses5556");
        when(redisTemplate.opsForValue()).thenReturn(ops);
        when(redisTemplate.delete(anyString())).thenReturn(true);
        when(otpUtil.decryptOTPCipher(anyString(), anyString())).thenReturn("+918092212300");
        when(contactNumberValidator.isValid(anyString(), anyString())).thenReturn(true);
        headers.add(Header.X_CHANNEL.getValue(), XChannel.BUYER.getValue());
        loginServiceImpl.processLogin(request, headers).getStatusCode();
    }

    @Test(expected = InvalidPayloadException.class)
    public void processLoginTest_whenOTPisInvalid() throws Exception {
        request.setPhoneNo("+918092212312");
        request.setOTP("091");
        request.setRefId("c9891bf9-e1ec-446e-aa6e-ebe384cf6849");
        request.setCountryCode("IN");
        ValueOperations ops = mock(ValueOperations.class);
        when(ops.get(anyString())).thenReturn("zfssawaw1seses5556");
        when(redisTemplate.opsForValue()).thenReturn(ops);
        when(redisTemplate.delete(anyString())).thenReturn(true);
        when(otpUtil.decryptOTPCipher(anyString(), anyString())).thenReturn("+918092212312");
        when(contactNumberValidator.isValid(anyString(), anyString())).thenReturn(true);
        headers.add(Header.X_CHANNEL.getValue(), XChannel.BUYER.getValue());
        loginServiceImpl.processLogin(request, headers).getStatusCode();
    }

    @Test(expected = InvalidPayloadException.class)
    public void processLoginTest_whenNoCipherTextWasFound() throws Exception {
        request.setPhoneNo("+918092212312");
        request.setOTP("0911");
        request.setRefId("c9891bf9-e1ec-446e-aa6e-ebe384cf6849");
        request.setCountryCode("IN");
        ValueOperations ops = mock(ValueOperations.class);
        when(ops.get(anyString())).thenReturn(null);
        when(redisTemplate.opsForValue()).thenReturn(ops);
        when(contactNumberValidator.isValid(anyString(), anyString())).thenReturn(true);
        headers.add(Header.X_CHANNEL.getValue(), XChannel.BUYER.getValue());
        loginServiceImpl.processLogin(request, headers).getStatusCode();
    }

    @Test(expected = InvalidPayloadException.class)
    public void processLoginTest_whenWithInvalidCountryCodeWithPhoneNumber() throws Exception {
        request.setPhoneNo("+918092212312");
        request.setOTP("0911");
        request.setRefId("c9891bf9-e1ec-446e-aa6e-ebe384cf6849");
        request.setCountryCode("US");
        when(contactNumberValidator.isValid(anyString(), anyString())).thenReturn(false);
        headers.add(Header.X_CHANNEL.getValue(), XChannel.BUYER.getValue());
        loginServiceImpl.processLogin(request, headers).getStatusCode();
    }

    @Test(expected = InvalidPayloadException.class)
    public void processLoginTest_whenInvalidRequestId() throws Exception {
        request.setPhoneNo("+918092212312");
        request.setOTP("0911");
        request.setRefId("e1ec-446e-aa6e");
        request.setCountryCode("IN");
        when(contactNumberValidator.isValid(anyString(), anyString())).thenReturn(true);
        headers.add(Header.X_CHANNEL.getValue(), XChannel.BUYER.getValue());
        loginServiceImpl.processLogin(request, headers).getStatusCode();
    }

    @Test(expected = InvalidPayloadException.class)
    public void processLoginTest_whenNoUserFound() throws Exception {
        request.setPhoneNo("+918092212312");
        request.setOTP("0911");
        request.setRefId("c9891bf9-e1ec-446e-aa6e-ebe384cf6849");
        request.setCountryCode("IN");
        ValueOperations ops = mock(ValueOperations.class);
        when(ops.get(anyString())).thenReturn("zfssawaw1seses5556");
        when(redisTemplate.opsForValue()).thenReturn(ops);
        when(redisTemplate.delete(anyString())).thenReturn(true);
        when(otpUtil.decryptOTPCipher(anyString(), anyString())).thenReturn("+918092212312");
        when(contactNumberValidator.isValid(anyString(), anyString())).thenReturn(true);
        headers.add(Header.X_CHANNEL.getValue(), XChannel.BUYER.getValue());
        when(customerDetailDAO.findByPhoneNo(anyString())).thenReturn(null);
        loginServiceImpl.processLogin(request, headers).getStatusCode();
    }

    @Test
    public void processLoginTest_WhenValidLogin_Request_HasNoTokenInCollection() throws Exception {
        request.setPhoneNo("+918092212312");
        request.setOTP("0911");
        request.setRefId("c9891bf9-e1ec-446e-aa6e-ebe384cf6849");
        request.setCountryCode("IN");
        restaurantDetail.setToken("");
        deliveryPersonDetail.setToken("");
        customerDetail.setToken("");
        ValueOperations ops = mock(ValueOperations.class);
        when(ops.get(anyString())).thenReturn("zfssawaw1seses5556");
        when(redisTemplate.opsForValue()).thenReturn(ops);
        when(redisTemplate.delete(anyString())).thenReturn(true);
        when(otpUtil.decryptOTPCipher(anyString(), anyString())).thenReturn("+918092212312");
        when(contactNumberValidator.isValid(anyString(), anyString())).thenReturn(true);
        when(customerDetailDAO.findByPhoneNo(anyString())).thenReturn(customerDetail);
        when(restaurantDetailDAO.findByPhoneNo(anyString())).thenReturn(restaurantDetail);
        when(deliveryPersonDetailDAO.findByPhoneNo(anyString())).thenReturn(deliveryPersonDetail);
        headers.add(Header.X_CHANNEL.getValue(), XChannel.BUYER.getValue());
        assertEquals(HttpStatus.OK, loginServiceImpl.processLogin(request, headers).getStatusCode());
        headers.remove(Header.X_CHANNEL.getValue());
        headers.add(Header.X_CHANNEL.getValue(), XChannel.SELLER.getValue());
        assertEquals(HttpStatus.OK, loginServiceImpl.processLogin(request, headers).getStatusCode());
        headers.remove(Header.X_CHANNEL.getValue());
        headers.add(Header.X_CHANNEL.getValue(), XChannel.TRANSPORT.getValue());
        assertEquals(HttpStatus.OK, loginServiceImpl.processLogin(request, headers).getStatusCode());
    }

    @Test(expected = InvalidPayloadException.class)
    public void processLoginTest_WhenValidLogin_Request_HasNoTokenInCollection_withAccountDisabled() throws Exception {
        request.setPhoneNo("+918092212312");
        request.setOTP("0911");
        request.setRefId("c9891bf9-e1ec-446e-aa6e-ebe384cf6849");
        request.setCountryCode("IN");
        ValueOperations ops = mock(ValueOperations.class);
        when(ops.get(anyString())).thenReturn("zfssawaw1seses5556");
        when(redisTemplate.opsForValue()).thenReturn(ops);
        when(redisTemplate.delete(anyString())).thenReturn(true);
        when(otpUtil.decryptOTPCipher(anyString(), anyString())).thenReturn("+918092212312");
        when(contactNumberValidator.isValid(anyString(), anyString())).thenReturn(true);
        customerDetail.setActive(false);
        when(customerDetailDAO.findByPhoneNo(anyString())).thenReturn(customerDetail);
        headers.add(Header.X_CHANNEL.getValue(), XChannel.BUYER.getValue());
        loginServiceImpl.processLogin(request, headers);
    }

    @Test(expected = InvalidHeaderValueException.class)
    public void processLoginTest_ForInvalidXChannel() throws Exception {
        doThrow(new InvalidHeaderValueException(ApplicationMessage.INVALID_OR_MISSING_API_KEY_OR_XCHANNEL_COMBINATION))
                .when(headerValidation).validateRequestHeader(any(), any());
        headers.add(Header.X_CHANNEL.getValue(), "XYZ");
        loginServiceImpl.processLogin(request, headers);
    }

    @Test
    public void processLoginCheck_forValidLoginRequest() throws Exception {
        LoginCheckRequest req = new LoginCheckRequest();
        req.setPhoneNo("+918092212312");
        req.setCountryCode("IN");
        HttpHeaders headers = new HttpHeaders();
        headers.set(Header.X_CORRELATION_ID.getValue(), UUID.randomUUID().toString());
        headers.add(Header.X_CHANNEL.getValue(), XChannel.BUYER.getValue());
        when(contactNumberValidator.isValid(anyString(), anyString())).thenReturn(true);
        ValueOperations<String, Object> ops = mock(ValueOperations.class);
        when(redisTemplate.opsForValue()).thenReturn(ops);
        doReturn("xyz").when(otpUtil).cipherForOTPWithPhoneNo(anyString(), anyString());
        doReturn(mock(CompletableFuture.class)).when(restExchangeUtil).execute(any(), any(), any(), any(), any());
        assertEquals(HttpStatus.OK, loginServiceImpl.processLoginCheck(req, headers).getStatusCode());
    }

    @Test(expected = InvalidPayloadException.class)
    public void processLoginCheck_forInvalidPayload() throws Exception {
        LoginCheckRequest req = new LoginCheckRequest();
        req.setPhoneNo("+918092212312");
        req.setCountryCode("US");
        HttpHeaders headers = new HttpHeaders();
        headers.set(Header.X_CORRELATION_ID.getValue(), UUID.randomUUID().toString());
        headers.add(Header.X_CHANNEL.getValue(), XChannel.BUYER.getValue());
        when(contactNumberValidator.isValid(anyString(), anyString())).thenReturn(false);
        loginServiceImpl.processLoginCheck(req, headers);
    }

    @Test(expected = InvalidPayloadException.class)
    public void processLoginCheck_forUserNotFoundInDB() throws Exception {
        LoginCheckRequest req = new LoginCheckRequest();
        req.setPhoneNo("+918092212312");
        req.setCountryCode("IN");
        HttpHeaders headers = new HttpHeaders();
        headers.set(Header.X_CORRELATION_ID.getValue(), UUID.randomUUID().toString());
        headers.add(Header.X_CHANNEL.getValue(), XChannel.BUYER.getValue());
        when(contactNumberValidator.isValid(anyString(), anyString())).thenReturn(true);
        when(customerDetailDAO.findByPhoneNo(anyString())).thenReturn(null);
        loginServiceImpl.processLoginCheck(req, headers);
    }

    @Test(expected = InvalidPayloadException.class)
    public void processLoginCheck_forUserNotFoundInInActive() throws Exception {
        LoginCheckRequest req = new LoginCheckRequest();
        req.setPhoneNo("+918092212312");
        req.setCountryCode("IN");
        HttpHeaders headers = new HttpHeaders();
        headers.set(Header.X_CORRELATION_ID.getValue(), UUID.randomUUID().toString());
        headers.add(Header.X_CHANNEL.getValue(), XChannel.BUYER.getValue());
        when(contactNumberValidator.isValid(anyString(), anyString())).thenReturn(true);
        customerDetail.setActive(false);
        when(customerDetailDAO.findByPhoneNo(anyString())).thenReturn(customerDetail);
        loginServiceImpl.processLoginCheck(req, headers);
    }

}