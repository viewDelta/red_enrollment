package com.red.ct.red_enrollment.helper;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.security.GeneralSecurityException;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class OtpUtilTest {

    private String phNo = "+918093397598", OTP = "1234";
    private final OtpUtil cryptUtil;

    public OtpUtilTest() throws GeneralSecurityException {
        cryptUtil = new OtpUtil();
    }

    @Test
    public void cipherForOTPWithPhoneNo() throws GeneralSecurityException {
        Assert.assertNotNull(cryptUtil.cipherForOTPWithPhoneNo(phNo, OTP));
    }

    @Test
    public void decryptOTPCipher() throws GeneralSecurityException {
        String cipherText = cryptUtil.cipherForOTPWithPhoneNo(phNo, OTP);
        Assert.assertEquals(phNo, cryptUtil.decryptOTPCipher(cipherText, OTP));
    }
}