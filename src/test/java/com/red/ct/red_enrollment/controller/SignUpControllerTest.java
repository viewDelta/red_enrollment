package com.red.ct.red_enrollment.controller;

import com.red.ct.domain.inbound.request.CreateConsumerRequest;
import com.red.ct.domain.inbound.request.CreateDeliveryPersonRequest;
import com.red.ct.domain.inbound.request.CreateRestaurantRequest;
import com.red.ct.domain.inbound.response.ApiResponse;
import com.red.ct.red_enrollment.service.ISignUpService;
import com.red.ct.utility.constants.ApplicationCode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SignUpControllerTest {

    @Mock
    private ISignUpService signUpService;

    @InjectMocks
    private SignUpController signUpController;

    @Test
    public void postConsumerTest_forValidRequest() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        when(signUpService.signUpForClient(any(CreateConsumerRequest.class), any(HttpHeaders.class))).thenReturn(getSuccessResponse());
        assertEquals(HttpStatus.OK, signUpController.postConsumer(mock(CreateConsumerRequest.class), mock(HttpHeaders.class)).getStatusCode());
    }

    @Test
    public void postConsumerTest_forInValidRequest() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        when(signUpService.signUpForClient(any(CreateConsumerRequest.class), any(HttpHeaders.class))).thenReturn(getFailureResponse());
        assertEquals(HttpStatus.BAD_REQUEST, signUpController.postConsumer(mock(CreateConsumerRequest.class), mock(HttpHeaders.class)).getStatusCode());
    }

    @Test
    public void postDeliveryPersonTest_forValidRequest() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        when(signUpService.signUpForDeliveryPerson(any(CreateDeliveryPersonRequest.class), any(HttpHeaders.class))).thenReturn(getSuccessResponse());
        assertEquals(HttpStatus.OK, signUpController.postDeliveryPerson(mock(CreateDeliveryPersonRequest.class), mock(HttpHeaders.class)).getStatusCode());
    }

    @Test
    public void postDeliveryPersonTest_forInValidRequest() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        when(signUpService.signUpForDeliveryPerson(any(CreateDeliveryPersonRequest.class), any(HttpHeaders.class))).thenReturn(getFailureResponse());
        assertEquals(HttpStatus.BAD_REQUEST, signUpController.postDeliveryPerson(mock(CreateDeliveryPersonRequest.class), mock(HttpHeaders.class)).getStatusCode());
    }

    @Test
    public void postRestaurantTest_forValidRequest() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        when(signUpService.signUpForRestaurant(any(CreateRestaurantRequest.class), any(HttpHeaders.class))).thenReturn(getSuccessResponse());
        assertEquals(HttpStatus.OK, signUpController.postRestaurant(mock(CreateRestaurantRequest.class), mock(HttpHeaders.class)).getStatusCode());
    }

    @Test
    public void postRestaurantTest_forInValidRequest() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        when(signUpService.signUpForRestaurant(any(CreateRestaurantRequest.class), any(HttpHeaders.class))).thenReturn(getFailureResponse());
        assertEquals(HttpStatus.BAD_REQUEST, signUpController.postRestaurant(mock(CreateRestaurantRequest.class), mock(HttpHeaders.class)).getStatusCode());
    }

    private ResponseEntity<ApiResponse> getSuccessResponse() {
        ApiResponse res = new ApiResponse(ApplicationCode.REQUEST_PROCESSED_SUCCESSFULLY.getCode(),
                ApplicationCode.REQUEST_PROCESSED_SUCCESSFULLY.getMsg());
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    private ResponseEntity<ApiResponse> getFailureResponse() {
        ApiResponse res = new ApiResponse(ApplicationCode.INVALID_PAYLOAD.getCode(),
                ApplicationCode.INVALID_PAYLOAD.getMsg());
        return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
    }
}