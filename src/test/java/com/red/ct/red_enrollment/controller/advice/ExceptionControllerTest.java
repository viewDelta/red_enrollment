package com.red.ct.red_enrollment.controller.advice;

import com.google.gson.Gson;
import com.red.ct.domain.inbound.request.LoginRequest;
import com.red.ct.exception.InvalidDateFormatException;
import com.red.ct.exception.InvalidHeaderValueException;
import com.red.ct.exception.InvalidPayloadException;
import com.red.ct.red_enrollment.controller.LoginController;
import com.red.ct.utility.constants.ApplicationCode;
import com.red.ct.utility.constants.EndPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.validation.ConstraintViolationException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class ExceptionControllerTest {

    private MockMvc mockMvc;

    @Mock
    private LoginController loginController;

    String requestJson;

    @Before
    public void setup() {
        LoginRequest req = new LoginRequest();
        req.setCountryCode("IM");
        req.setOTP("0979");
        req.setPhoneNo("+918909090908");
        requestJson = new Gson().toJson(req);
        this.mockMvc = MockMvcBuilders.standaloneSetup(loginController)
                .setControllerAdvice(new ExceptionController())
                .build();
    }

    @Test
    public void test_handleException_forInternalServerError() throws Exception {
        when(loginController.login(any(), any())).thenThrow(mock(Exception.class));
        mockMvc.perform(post(EndPoint.LOGIN)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(requestJson))
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentTypeCompatibleWith("application/json"))
                .andExpect(jsonPath("$.code").value(ApplicationCode.INTERNAL_SERVER_ERROR.getCode()))
                .andExpect(jsonPath("$.msg").value(ApplicationCode.INTERNAL_SERVER_ERROR.getMsg()));
    }

    @Test
    public void test_handleException_forInvalidPayload() throws Exception {
        when(loginController.login(any(), any())).thenThrow(mock(InvalidPayloadException.class));
        mockMvc.perform(post(EndPoint.LOGIN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith("application/json"))
                .andExpect(jsonPath("$.code").value(ApplicationCode.INVALID_PAYLOAD.getCode()));
    }

    @Test
    public void test_handleException_forConstraintViolationException() throws Exception {
        when(loginController.login(any(), any())).thenThrow(mock(ConstraintViolationException.class));
        mockMvc.perform(post(EndPoint.LOGIN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith("application/json"))
                .andExpect(jsonPath("$.code").value(ApplicationCode.INVALID_PAYLOAD.getCode()));
    }

    @Test
    public void test_handleException_forInvalidDateFormatException() throws Exception {
        when(loginController.login(any(), any())).thenThrow(mock(InvalidDateFormatException.class));
        mockMvc.perform(post(EndPoint.LOGIN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith("application/json"))
                .andExpect(jsonPath("$.code").value(ApplicationCode.INVALID_PAYLOAD.getCode()));
    }

    @Test
    public void test_handleException_forInvalidHeaderValue() throws Exception {
        when(loginController.login(any(), any())).thenThrow(mock(InvalidHeaderValueException.class));
        mockMvc.perform(post(EndPoint.LOGIN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isForbidden())
                .andExpect(content().contentTypeCompatibleWith("application/json"))
                .andExpect(jsonPath("$.code").value(ApplicationCode.INVALID_OR_MISSING_API_KEY_OR_X_CHANNEL.getCode()));
    }
}