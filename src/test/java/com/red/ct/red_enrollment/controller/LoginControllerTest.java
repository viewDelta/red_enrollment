package com.red.ct.red_enrollment.controller;

import com.red.ct.domain.inbound.request.LoginCheckRequest;
import com.red.ct.domain.inbound.request.LoginRequest;
import com.red.ct.domain.inbound.response.ApiResponse;
import com.red.ct.red_enrollment.service.ILoginService;
import com.red.ct.utility.constants.ApplicationCode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class LoginControllerTest {

    @Mock
    private ILoginService loginService;

    @InjectMocks
    private LoginController loginController;

    @Test
    public void loginTest_forValidRequest() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        when(loginService.processLogin(any(), any())).thenReturn(getSuccessResponse());
        assertEquals(HttpStatus.OK ,loginController.login(mock(LoginRequest.class), mock(HttpHeaders.class)).getStatusCode());
    }

    @Test
    public void loginTest_forInValidRequest() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        when(loginService.processLogin(any(), any())).thenReturn(getFailureResponse());
        assertEquals(HttpStatus.BAD_REQUEST ,loginController.login(mock(LoginRequest.class), mock(HttpHeaders.class)).getStatusCode());
    }

    @Test
    public void loginCheckTest_forValidRequest() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        when(loginService.processLoginCheck(any(), any())).thenReturn(getSuccessResponse());
        assertEquals(HttpStatus.OK ,loginController.loginCheck(mock(LoginCheckRequest.class), mock(HttpHeaders.class)).getStatusCode());
    }

    @Test
    public void loginCheckTest_forInValidRequest() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        when(loginService.processLoginCheck(any(), any())).thenReturn(getFailureResponse());
        assertEquals(HttpStatus.BAD_REQUEST ,loginController.loginCheck(mock(LoginCheckRequest.class), mock(HttpHeaders.class)).getStatusCode());
    }

    private ResponseEntity<ApiResponse> getSuccessResponse(){
        ApiResponse res = new ApiResponse(ApplicationCode.REQUEST_PROCESSED_SUCCESSFULLY.getCode(),
                ApplicationCode.REQUEST_PROCESSED_SUCCESSFULLY.getMsg());
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    private ResponseEntity<ApiResponse> getFailureResponse(){
        ApiResponse res = new ApiResponse(ApplicationCode.INVALID_PAYLOAD.getCode(),
                ApplicationCode.INVALID_PAYLOAD.getMsg());
        return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
    }
}